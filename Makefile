BUILDDIR=build
SRCDIR=$(SOLUTIONDIR)/src
INCDIR=$(SOLUTIONDIR)/include
TESTDIR=test_results
SOLUTIONDIR=solution
CC=gcc
CFLAGS=--std=c17 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG -O3 -I$(INCDIR)

ASMC=nasm
ASMFLAGS=-felf64 -g

LINK=ld
LINKFLAGS =-lm -no-pie

all: $(BUILDDIR)/padding.o $(BUILDDIR)/bmp_header.o $(BUILDDIR)/error_printer.o $(BUILDDIR)/file_manager.o $(BUILDDIR)/util.o $(BUILDDIR)/bmp_file_io.o $(BUILDDIR)/image.o $(BUILDDIR)/sepia.o $(BUILDDIR)/sepia_sse.o $(BUILDDIR)/main.o
	$(CC) $(LINKFLAGS) -o $(BUILDDIR)/image_sepia $^

build:
	mkdir -p $(BUILDDIR)

tests:
	mkdir -p $(TESTDIR)

$(BUILDDIR)/padding.o: $(SRCDIR)/padding.c build
	$(CC) -c $(CFLAGS) $(HEADDIR) $< -o $@

$(BUILDDIR)/bmp_header.o: $(SRCDIR)/bmp_header.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/error_printer.o: $(SRCDIR)/error_printer.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/file_manager.o: $(SRCDIR)/file_manager.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/util.o: $(SRCDIR)/util.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/bmp_file_io.o: $(SRCDIR)/bmp_file_io.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/image.o: $(SRCDIR)/image.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/sepia.o: $(SRCDIR)/sepia.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/main.o: $(SRCDIR)/main.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/sepia_sse.o: $(SRCDIR)/sepia_sse.asm build
	$(ASMC) $(ASMFLAGS) $< -o $@

run:
	$(BUILDDIR)/image_sepia

test: all tests
	# run C
	$(BUILDDIR)/image_sepia C input.bmp $(TESTDIR)/output_c.bmp
	# run ASM
	$(BUILDDIR)/image_sepia ASM input.bmp $(TESTDIR)/output_asm.bmp
	# run timetest
	$(BUILDDIR)/image_sepia test input.bmp $(TESTDIR)/output.bmp

clean:
	rm -rf $(BUILDDIR)
	rm -rf $(TESTDIR)

.PHONY: all test run