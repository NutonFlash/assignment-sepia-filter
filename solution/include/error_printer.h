#ifndef ERROR_PRINTER_H
#define ERROR_PRINTER_H

enum error_code {
    ERROR_INVALID_SIGNATURE,
    ERROR_INVALID_HEADER,
    ERROR_INVALID_BITS ,
    ERROR_NULL_PTR,
    ERROR_WRITE,
    ERROR_CLOSE,
    ERROR_OPEN,
    ERROR_ARGS,
    ERROR_MEMORY_ALLOCATION
};

void print_error(enum error_code code);

#endif
