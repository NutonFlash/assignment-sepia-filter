#ifndef _SEPIA_H_
#define _SEPIA_H_
#include "image.h"
struct image sepia_c_inplace(struct image src);
struct image sepia_asm_inplace(struct image img);
#endif
