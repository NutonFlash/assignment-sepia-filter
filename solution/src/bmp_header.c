#include "../include/bmp_header.h"
#include "../include/padding.h"

#include <stdint.h>

struct bmp_header header_from_image(const struct image* img) {
    struct bmp_header header = {0};
    header.bfType = HEADER_BF_TYPE;
    header.bfileSize = sizeof (struct bmp_header) + image_get_size_bytes(img) + padding_size_bytes(img);
    header.bOffBits = HEADER_OFF_BITS;
    header.biSize = HEADER_BI_SIZE;
    header.bfReserved = HEADER_BF_RESERVED;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = HEADER_BI_PLANES;
    header.biBitCount = HEADER_BI_BIT_COUNT;
    header.biCompression = HEADER_BI_COMPRESSION;
    header.biSizeImage = header.bfileSize - HEADER_OFF_BITS;
    header.biXPelsPerMeter = HEADER_BI_X_PPM;
    header.biYPelsPerMeter = HEADER_BI_Y_PPM;
    header.biClrUsed = HEADER_CLR_USED;
    header.biClrImportant = HEADER_CLR_IMP;
    return header;
}

bool header_size_valid(struct bmp_header const* header) {
    return header->biHeight > 0 && header->biWidth > 0;
}

bool header_signature_valid(struct bmp_header const* header) {
    return header->bfType == HEADER_BF_TYPE;
}

