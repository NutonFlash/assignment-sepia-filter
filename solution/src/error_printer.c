#include "../include/error_printer.h"

#include <stdio.h>

static const char* error_outputs[] = {
        [ERROR_INVALID_SIGNATURE] = "Файл не является BMP.",
        [ERROR_INVALID_HEADER] = "Невалидный заголовок BMP файла.",
        [ERROR_INVALID_BITS] = "Невалидное изображение в файле.",
        [ERROR_NULL_PTR] = "Непредвиденная ошибка.",
        [ERROR_WRITE] = "Ошибка при записи в файл.",
        [ERROR_CLOSE] = "Ошибка при закрытии файлов.",
        [ERROR_OPEN] = "Ошибка при открытии файлов.",
        [ERROR_ARGS] = "Для запуска программы необходимо указать 3 аргумента:\n1)C|ASM|test\n2)путь до входного файла\n3)путь до выходного файла",
        [ERROR_MEMORY_ALLOCATION] = "Ошибка выделении памяти при создании картинки."
};

void print_error(enum error_code code) {
    fprintf(stderr, "%s %s", error_outputs[code], "\n");
}


