#include "../include/file_manager.h"

#include <stdbool.h>
#include <stdio.h>

bool open_file(FILE** file, char const* name, char const* mode) {
    if (name == NULL)
        return false;
    *file = fopen(name, mode);
    return *file;
}

bool close_file(FILE *file) {
    if (file == NULL)
        return false;
    return !fclose(file);
}
