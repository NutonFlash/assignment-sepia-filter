#include "../include/bmp_header.h"
#include "../include/bmp_file_io.h"
#include "../include/error_printer.h"
#include "../include/image.h"
#include "../include/sepia.h"

#include <sys/time.h>
#include <sys/resource.h>

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

static const unsigned read_error_codes[] = {
        [READ_INVALID_SIGNATURE] = ERROR_INVALID_SIGNATURE,
        [READ_INVALID_HEADER] = ERROR_INVALID_HEADER,
        [READ_INVALID_BITS] = ERROR_INVALID_BITS,
        [READ_NULL_PTR] = ERROR_NULL_PTR,
        [ALLOCATE_DATA_ERROR] = ERROR_MEMORY_ALLOCATION
};

bool open_file(FILE** file, char const* name, char const* mode);
bool close_file(FILE *file);
int strcmp (const char *str1, const char *str2);

int main( int argc, char** argv ) {
    FILE* in = NULL;
    FILE* out = NULL;
    struct image img = {0};
    struct image sepia_img;
    enum read_status read_status;

    if (argc != 4) {
        print_error(ERROR_ARGS);
        return 1;
    }
    if (!(open_file(&in, argv[2], "rb") && open_file(&out, argv[3], "wb"))) {
        print_error(ERROR_OPEN);
        return 1;
    }
    read_status = from_bmp(in, &img);
    if (read_status != READ_OK) {
        print_error(read_error_codes[read_status]);
        image_destroy(&img);
        close_file(in);
        close_file(out);
        return 1;
    } else {
        if(strcmp(argv[1], "C") == 0) 
        {
            printf("Программа будет использовать C для изменения пикселей картинки.\n");
            sepia_img = sepia_c_inplace(img);
        }
        else if(strcmp(argv[1], "ASM") == 0) 
        {
            printf("Программа будет использовать ASM для изменения пикселей картинки.\n");
            sepia_img = sepia_asm_inplace(img);
        }
        else if(strcmp(argv[1], "test") == 0) 
        {
            printf("Программа протестирует скорость выполнения кода на C и на ASM.\n");
            struct image new_img = *image_copy(img);

            uint64_t avg_c_time = 0, avg_asm_time = 0;
            for (int i = 0; i < 10; i++) {
                struct timeval t0, t1, t2;

                gettimeofday(&t0, 0);

                sepia_c_inplace(new_img);
                gettimeofday(&t1, 0);

                sepia_asm_inplace(new_img);
                gettimeofday(&t2, 0);

                avg_c_time += (t1.tv_sec - t0.tv_sec) * 1000000L + t1.tv_usec - t0.tv_usec;
                avg_asm_time += (t2.tv_sec - t1.tv_sec) * 1000000L + t2.tv_usec - t1.tv_usec;
            }
            printf("Среднее время выполнения на C: %.1f\n", avg_c_time / 10.0);
            printf("Среднее время выполнения на ASM: %.1f\n", avg_asm_time / 10.0);
        }
        else {
            print_error(ERROR_ARGS);
            image_destroy(&img);
            close_file(in);
            close_file(out);
            return 1;
        }
    }
    image_destroy(&img);
    if (to_bmp(out, &sepia_img)) {
        print_error(ERROR_WRITE);
        image_destroy(&sepia_img);
        close_file(in);
        close_file(out);
        return 1;
    } else printf("Сепия-фильтр успешно наложен на картинку!\n");
    image_destroy(&sepia_img);
    if (!(close_file(in) && close_file(out))) {
        print_error(ERROR_CLOSE);
        return 1;
    }
    return 0;
}


