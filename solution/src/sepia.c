#include "../include/sepia.h"
#include "../include/image.h"
#include "../include/util.h"
#include <inttypes.h>
#include <stdint.h>
#include <string.h>

static unsigned char sat(uint64_t x){
	if(x < 256) return x; 
    return 255;
}

inline struct pixel* pixel_of(const struct image img, const uint64_t x, const uint64_t y) {
	return &img.data[y*img.width+x];
}

static void sepia_one( struct pixel *const pixel ) {
  static const float c[3][3] = {
     { .393f, .769f, .189f },
     { .349f, .686f, .168f },
     { .272f, .543f, .131f }
  };
  struct pixel const old = *pixel;
  pixel->r = sat(
    byte_to_float[old.r] * c[0][0] + 
    byte_to_float[old.g] * c[0][1] + 
    byte_to_float[old.b] * c[0][2]);
  pixel->g = sat( 
    byte_to_float[old.r] * c[1][0] + 
    byte_to_float[old.g] * c[1][1] + 
    byte_to_float[old.b] * c[1][2]);
  pixel->b = sat(
    byte_to_float[old.r] * c[2][0] + 
    byte_to_float[old.g] * c[2][1] + 
    byte_to_float[old.b] * c[2][2]);
}

struct image sepia_c_inplace(struct image img) {
    struct image new_image = *image_copy(img);
    uint32_t x, y;
    for ( y = 0; y < new_image.height; y++ ) {
        for ( x = 0; x < new_image.width; x++) {
		    sepia_one( pixel_of( new_image, x, y ) ); 
	    } 
    }
	return new_image;
}

extern void sepia_four_pixels(float blue[4], float green[4], float red[4], struct pixel * res);

struct image sepia_asm_inplace(struct image img){
    struct image new_image = *image_copy(img);
    const uint64_t size = new_image.width * new_image.height;
    uint64_t i = 0;
    for(; i < size; i += 4){
        float r[4], g[4], b[4];
        for (int j = 0; j < 4; j++){
            r[j] = byte_to_float[new_image.data[i+j].r];
            g[j] = byte_to_float[new_image.data[i+j].g];
            b[j] = byte_to_float[new_image.data[i+j].b];
        }
        sepia_four_pixels(b, g, r, &new_image.data[i]);
    }
    // Ну и последние пиксели без ассемблера преобразуем
    for (; i < size; i ++) {
        sepia_one(pixel_of(new_image, i, new_image.height));
    }
    return new_image;
}