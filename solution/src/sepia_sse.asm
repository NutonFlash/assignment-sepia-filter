global sepia_four_pixels

section .rodata
align 16
red: dd 0.272, 0.349, 0.393, 0.0
align 16
green: dd 0.543, 0.686, 0.769, 0.0
align 16
blue: dd 0.131, 0.168, 0.189, 0.0
align 16
max: dd 255.0, 255.0, 255.0, 255.0


%macro calc 2
    movdqu xmm0, [rdi]
    movdqu xmm1, [rsi]
    movdqu xmm2, [rdx]
    movdqu xmm3, [blue]
    movdqu xmm4, [green]
    movdqu xmm5, [red]

    shufps xmm0, xmm0, %1
    shufps xmm1, xmm1, %1
    shufps xmm2, xmm2, %1
    shufps xmm3, xmm3, %2
    shufps xmm4, xmm4, %2
    shufps xmm5, xmm5, %2
    
    mulps xmm0, xmm3
    mulps xmm1, xmm4
    mulps xmm2, xmm5
    addps xmm0, xmm1
    addps xmm0, xmm2
    
    ; save result
    minps xmm0, [max]
    cvtps2dq xmm0, xmm0
    pextrb [rcx], xmm0, 0
    pextrb [rcx+1], xmm0, 4
    pextrb [rcx+2], xmm0, 8
    pextrb [rcx+3], xmm0, 12
%endmacro


section .text
sepia_four_pixels:
    calc 0b_01_00_00_00, 0b_00_10_01_00
    add rcx, 4
    calc 0b_10_10_01_01, 0b_01_00_10_01
    add rcx, 4
    calc 0b_11_11_11_10, 0b_10_01_00_10
    ret